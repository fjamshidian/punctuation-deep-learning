
import numpy as np
import torch
from torch import nn
import torch.nn.functional as F
import string

# 1:loading trained model, 0: use the new trained model
testflg = 1

# 1: training a new model, 0: testing a saved model
trainflg = 0

# load whitespaces from string
puncData = string.punctuation

if trainflg == 1:
    with open('mydata.txt', "rb") as f:#, encoding="utf8", errors='ignore') as f:
        text = f.read()
    text = text.decode("utf-8", errors='ignore')
    
    # encode the text and map each character to an integer and vice versa
    text = text.replace("@"," ") #use @ as uppercase flag
    text = text.replace("#"," ") #use # in the case that no operation is needed
    rawtext = " ".join(text.split())
    
    # create two dictionaries:
    # 1. int2char, which maps integers to characters
    # 2. char2int, which maps characters to unique integers
    
    chars = tuple(set(rawtext))
    int2char = dict(enumerate(chars,start=2))#0 for all chars, 1 for uppercase
    char2int = {ch: ii for ii, ch in int2char.items()}
    charsList = list(chars)
    charsList.append('@')
    charsList.append('#')
    chars = tuple(charsList)
    int2char[1] = '@'
    int2char[0] = '#'
    char2int['@'] = 1
    char2int['#'] = 0
    
    #remove all whitespaces and punctuation, and convert string into lowercase
    text = ''.join(rawtext.split())
    text = ''.join(ch for ch in text if ch not in puncData)
    text = text.lower()
    
    #create the encoded target and data
    targetdata = np.zeros(len(text))
    i = 0
    for ch in rawtext[1:]:
      if ch.isspace():
        targetdata[i] = char2int[ch]
      elif puncData.find(ch)!=-1:
        targetdata[i] = char2int[ch]
      else:
        i = i+1
        if ch.isupper():
          targetdata[i] = 1
    encodedtar = np.array(targetdata)
    encoded = np.array([char2int[ch] for ch in text])
    
''' Embedding char to unique code
    
    Arguments
    ---------
    arr: input array of data
    n_labels: unique code to use
'''
def one_hot_encode(arr, n_labels):
    
    # Initialize the the encoded array
    one_hot = np.zeros((arr.size, n_labels), dtype=np.float32)
    
    # Fill the appropriate elements with ones
    one_hot[np.arange(one_hot.shape[0]), arr.flatten()] = 1.
    
    # Finally reshape it to get back to the original array
    one_hot = one_hot.reshape((*arr.shape, n_labels))
    
    return one_hot

'''Create a generator that returns batches of size
    batch_size x seq_length from arr.
    
    Arguments
    ---------
    arr: Array you want to make batches from
    batch_size: Batch size, the number of sequences per batch
    seq_length: Number of encoded chars in a sequence
'''
def get_batches(arr, tararr, batch_size, seq_length):

    batch_size_total = batch_size * seq_length
    # total number of batches we can make
    n_batches = len(arr)//batch_size_total
    
    # Keep only enough characters to make full batches
    arr = arr[:n_batches * batch_size_total]
    tararr = tararr[:n_batches * batch_size_total]
    
    # Reshape into batch_size rows
    arr = arr.reshape((batch_size, -1))
    tararr = tararr.reshape((batch_size, -1))
    
    # iterate through the array, one sequence at a time
    for n in range(0, arr.shape[1], seq_length):
        # The features
        x = arr[:, n:n+seq_length]
        # The targets
        y = tararr[:, n:n+seq_length]
        yield x, y

# check if GPU is available
train_on_gpu = torch.cuda.is_available()
if(train_on_gpu):
    print('Training on GPU!')
else: 
    print('No GPU available, training on CPU.')

class CharRNN(nn.Module):
    
    def __init__(self, tokens, ch2in, in2ch, n_hidden=256, n_layers=2,
                               drop_prob=0.5, lr=0.001):
        super().__init__()
        self.drop_prob = drop_prob
        self.n_layers = n_layers
        self.n_hidden = n_hidden
        self.lr = lr
        
        # creating character dictionaries
        self.chars = tokens
        self.int2char = in2ch 
        self.char2int = ch2in
        
        ## define the LSTM
        self.lstm = nn.LSTM(len(self.chars), n_hidden, n_layers, 
                            dropout=drop_prob, batch_first=True)
        
        ## define a dropout layer
        self.dropout = nn.Dropout(drop_prob)
        
        ## define the final, fully-connected output layer
        self.fc = nn.Linear(n_hidden, len(self.chars))
      
    
    def forward(self, x, hidden):
        ''' Forward pass through the network. 
            These inputs are x, and the hidden/cell state `hidden`. '''
                
        ## Get the outputs and the new hidden state from the lstm
        r_output, hidden = self.lstm(x, hidden)
        
        ## pass through a dropout layer
        out = self.dropout(r_output)
        
        # Stack up LSTM outputs using view
        # you may need to use contiguous to reshape the output
        out = out.contiguous().view(-1, self.n_hidden)
        
        ## put x through the fully-connected layer
        out = self.fc(out)
        
        # return the final output and the hidden state
        return out, hidden
    
    
    def init_hidden(self, batch_size):
        ''' Initializes hidden state '''
        # Create two new tensors with sizes n_layers x batch_size x n_hidden,
        # initialized to zero, for hidden state and cell state of LSTM
        weight = next(self.parameters()).data
        
        if (train_on_gpu):
            hidden = (weight.new(self.n_layers, batch_size, self.n_hidden).zero_().cuda(),
                  weight.new(self.n_layers, batch_size, self.n_hidden).zero_().cuda())
        else:
            hidden = (weight.new(self.n_layers, batch_size, self.n_hidden).zero_(),
                      weight.new(self.n_layers, batch_size, self.n_hidden).zero_())
        
        return hidden


def train(net, data, tardata, epochs=10, batch_size=10, seq_length=50, lr=0.001, clip=5, val_frac=0.1, print_every=10):
    ''' Training a network 
    
        Arguments
        ---------
        
        net: CharRNN network
        data: text data to train the network
        epochs: Number of epochs to train
        batch_size: Number of mini-sequences per mini-batch, aka batch size
        seq_length: Number of character steps per mini-batch
        lr: learning rate
        clip: gradient clipping
        val_frac: Fraction of data to hold out for validation
        print_every: Number of steps for printing training and validation loss
    
    '''
    print("training...")
    net.train()
    
    opt = torch.optim.Adam(net.parameters(), lr=lr)
    criterion = nn.CrossEntropyLoss()
    
    # create training and validation data
    val_idx = int(len(data)*(1-val_frac))
    data, val_data = data[:val_idx], data[val_idx:]
    tardata, tarval_data = tardata[:val_idx], tardata[val_idx:]
    
    if(train_on_gpu):
        net.cuda()
    
    counter = 0
    n_chars = len(net.chars)
    for e in range(epochs):
        # initialize hidden state
        h = net.init_hidden(batch_size)
        
        for x, y in get_batches(data, tardata, batch_size, seq_length):
            counter += 1
            # One-hot encode data and make them Torch tensors
            x = one_hot_encode(x, n_chars)
            inputs, targets = torch.from_numpy(x), torch.from_numpy(y)
            
            if(train_on_gpu):
                inputs, targets = inputs.cuda(), targets.cuda()

            # Creating new variables for the hidden state, otherwise
            # backprop through the entire training history
            h = tuple([each.data for each in h])

            # zero accumulated gradients
            net.zero_grad()
            
            # get the output from the model
            output, h = net(inputs, h)
            
            # calculate the loss and perform backprop
            loss = criterion(output, targets.view(batch_size*seq_length).long())
            loss.backward()
            # prevent the exploding gradient problem in RNNs / LSTMs.
            nn.utils.clip_grad_norm_(net.parameters(), clip)
            opt.step()
            
            # loss stats
            if counter % print_every == 0:
                # Get validation loss
                val_h = net.init_hidden(batch_size)
                val_losses = []
                net.eval()
                for x, y in get_batches(val_data, tarval_data, batch_size, seq_length):
                    # One-hot encode data and make them Torch tensors
                    x = one_hot_encode(x, n_chars)
                    x, y = torch.from_numpy(x), torch.from_numpy(y)
                    
                    # Creating new variables for the hidden state, otherwise
                    # backprop through the entire training history
                    val_h = tuple([each.data for each in val_h])
                    
                    inputs, targets = x, y
                    if(train_on_gpu):
                        inputs, targets = inputs.cuda(), targets.cuda()

                    output, val_h = net(inputs, val_h)
                    val_loss = criterion(output, targets.view(batch_size*seq_length).long())
                
                    val_losses.append(val_loss.item())
                
                net.train() # reset to train mode after iterationg through validation data
                
                print("Epoch: {}/{}...".format(e+1, epochs),
                      "Step: {}...".format(counter),
                      "Loss: {:.4f}...".format(loss.item()),
                      "Val Loss: {:.4f}".format(np.mean(val_losses)))
if trainflg == 1:
    # define and print the net
    n_hidden=512
    n_layers=2

    net = CharRNN(chars, char2int, int2char, n_hidden, n_layers)
    print(net)

    batch_size = 128
    seq_length = 100
    n_epochs = 20 


    # train the model
    train(net, encoded, encodedtar, epochs=n_epochs, batch_size=batch_size, seq_length=seq_length, lr=0.001, print_every=10)


    # change the name, for saving multiple files
    model_name = 'rnn_20_epoch_punctuation_dicMydata.net'

    checkpoint = {'n_hidden': net.n_hidden,
                  'n_layers': net.n_layers,
                  'state_dict': net.state_dict(),
                  'tokens': net.chars,
                  'ch2in': net.char2int,
                  'in2ch': net.int2char}

    with open(model_name, 'wb') as f:
        torch.save(checkpoint, f)




''' Predict the missed punctuations
    
    Arguments
    
    ---------
        
    net: CharRNN network
    ch: current char
    h: initialized hiiden states
    top_k: top charactes
    
    
'''
def predict(net, char, h=None, top_k=None):
        ''' Given a character, predict the next character.
            Returns the predicted character and the hidden state.
        '''
        # tensor inputs
        x = np.array([[net.char2int[char]]])
        x = one_hot_encode(x, len(net.chars))
        inputs = torch.from_numpy(x)
        
        if(train_on_gpu):
            inputs = inputs.cuda()
        
        # detach hidden state from history
        h = tuple([each.data for each in h])
        # get the output of the model
        out, h = net(inputs, h)

        # get the character probabilities
        p = F.softmax(out, dim=1).data
        if(train_on_gpu):
            p = p.cpu() # move to cpu
        
        # get top characters
        if top_k is None:
            top_ch = np.arange(len(net.chars))
        else:
            p, top_ch = p.topk(top_k)
            top_ch = top_ch.numpy().squeeze()
        
        # select the likely next character with some element of randomness
        p = p.numpy().squeeze()
        char = np.random.choice(top_ch, p=p/p.sum())
        
        # return the encoded value of the predicted char and the hidden state
        return char, h

''' Predict the missed punctuations
    
    Arguments
    
    ---------
        
    net: CharRNN network
    size: size
    filename: test filename
    top_k: top charactes
    
    
'''        

def sample(net, size, filename='story.txt', top_k=None):
        
    if(train_on_gpu):
        net.cuda()
    else:
        net.cpu()
    
    # open text file and read in data as `text`
    with open(filename, 'r') as f:
      storydata = f.read()
    #storydata = storydata[:300]
    storydata =storydata.replace("\n"," ")
    net.eval() # eval mode

    
    # First off, run through the prime characters
    chars1 = [ch for ch in "North rich mond street"]
    h = net.init_hidden(1)
    for ch in chars1:
        char, h = predict(net, ch, h, top_k=top_k)
        #chars.append(ch)

    
    # Now pass in the previous character and get a new one
    for ch in storydata[20:]:
      char, h = predict(net, ch, h, top_k=top_k)
      #print(''.join((chars1[-10:]))+ " char= ",char, " ", net.int2char[char],"  ch= "+ch)
      if char == 0:           # '#': # no operation is needed
          chars1.append(ch)
      elif char == 1:         # '@': # we had to upper case the letter
          chars1.append(ch.upper())
      elif puncData.find(net.int2char[char])!=-1:
          chars1.append(ch)
          chars1.append(net.int2char[char])
      elif (net.int2char[char]).isspace(): # space is detected
          chars1.append(ch)
          chars1.append(net.int2char[char])
    return ''.join((chars1))
 

if testflg == 1:
    
    # loadding the trained model
    with open('rnn_20_epoch_punctuation_dicMydata.net', 'rb') as f:
        checkpoint = torch.load(f)
    
     
    loaded = CharRNN(checkpoint['tokens'], checkpoint['ch2in'],checkpoint['in2ch'], n_hidden=checkpoint['n_hidden'], n_layers=checkpoint['n_layers'])
    loaded.load_state_dict(checkpoint['state_dict'])
    net = loaded
    print(sample(net, 0, filename='story.txt', top_k=5))
