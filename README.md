# Punctuation-Deep learning

In this punctuation restoration project, several missed punctuations are restored. Such as capitalization, comma, full stop, all whitespaces.

**Files:**


- mydata.txt: pre-processed data for training

- output.txt: output Loss according to the epoches

- PuncPrj.py:the main file of project

- readDataset.py: pre-processing on data to assemble them in a single file named mydata.txt

- reuters21578.tar.gz: standard data used in training

- result.txt: accuracy result

- rnn_20_epoch_punctuation_dicMydata: trained model

- story.txt: sample test data


**Requirements:**
```
numpy, torch, numpy, string
```

**Performance:**

Train and test Loss for 20 epoches
```
Epoch: 1/20        Step: 10          Loss: 1.0651       Val Loss: 1.1593
Epoch: 1/20        Step: 1060        Loss: 0.5305       Val Loss: 0.5160
Epoch: 2/20        Step: 2130        Loss: 0.3854       Val Loss: 0.3977
Epoch: 3/20        Step: 3200        Loss: 0.3101       Val Loss: 0.3498
Epoch: 4/20        Step: 4270        Loss: 0.3075       Val Loss: 0.3280
Epoch: 5/20        Step: 5340        Loss: 0.2767       Val Loss: 0.3165
Epoch: 6/20        Step: 6400        Loss: 0.2443       Val Loss: 0.3181
Epoch: 7/20        Step: 7470        Loss: 0.2513       Val Loss: 0.3063
Epoch: 8/20        Step: 8540        Loss: 0.2123       Val Loss: 0.2823
Epoch: 9/20        Step: 9610        Loss: 0.2350       Val Loss: 0.2960
Epoch: 10/20       Step: 10680       Loss: 0.2223       Val Loss: 0.3066
Epoch: 11/20       Step: 11740       Loss: 0.2003       Val Loss: 0.2934
Epoch: 12/20       Step: 12810       Loss: 0.2153       Val Loss: 0.3021
Epoch: 13/20       Step: 13880       Loss: 0.1755       Val Loss: 0.3015
Epoch: 14/20       Step: 14950       Loss: 0.1999       Val Loss: 0.2823
Epoch: 15/20       Step: 16020       Loss: 0.2012       Val Loss: 0.2993
Epoch: 16/20       Step: 17080       Loss: 0.1834       Val Loss: 0.2837
Epoch: 17/20       Step: 18150       Loss: 0.1920       Val Loss: 0.3115
Epoch: 18/20       Step: 19220       Loss: 0.1668       Val Loss: 0.3066
Epoch: 19/20       Step: 20290       Loss: 0.1858       Val Loss: 0.3253
Epoch: 20/20       Step: 21360       Loss: 0.1865       Val Loss: 0.3190
```


**Functions:**

- train()

Training a network
``` 
    Arguments
    ---------
    
    net: CharRNN network
    data: text data to train the network
    epochs: Number of epochs to train
    batch_size: Number of mini-sequences per mini-batch, aka batch size
    seq_length: Number of character steps per mini-batch
    lr: learning rate
    clip: gradient clipping
    val_frac: Fraction of data to hold out for validation
    print_every: Number of steps for printing training and validation loss

```

- sample()

Predict the missed punctuations
``` 
    Arguments
    
    ---------
        
    net: CharRNN network
    size: size
    filename: test filename
    top_k: top charactes
    
``` 



- one_hot_encode()

Embedding char to unique code
```
    
    Arguments
    ---------
    arr: input array of data
    n_labels: unique code to use

```


- get_batches()

Create a generator that returns batches of size
```
    batch_size x seq_length from arr.
    
    Arguments
    ---------
    arr: Array you want to make batches from
    batch_size: Batch size, the number of sequences per batch
    seq_length: Number of encoded chars in a sequence

```



- predict()

Predict the missed punctuations
```    
    Arguments
    
    ---------
        
    net: CharRNN network
    ch: current char
    h: initialized hiiden states
    top_k: top charactes
    
```

