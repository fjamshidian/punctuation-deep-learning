from bs4 import BeautifulSoup,SoupStrainer

import os
datatext = ""
# for every data in the folder
for file in os.listdir("reuters21578/"):
    
    if file.endswith(".sgm"):
        # set path
        filepath = "reuters21578/" + file
        f = open(filepath, 'r')
        data= f.read()
        soup = BeautifulSoup(data,features="lxml")
        contents = soup.findAll('body')
        # merge data together
        for content in contents:
            datatext = datatext + content.text

print(datatext)
# saving ...
datafile = open("mydata.txt","w")
datafile.write(datatext)
datafile.close()


